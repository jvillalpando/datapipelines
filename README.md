# Redshift
A continuación hay una serie de links con temas utiles acerca de Redshift:

- [Overview](https://docs.aws.amazon.com/redshift/latest/dg/c_redshift_system_overview.html)
- [Introducción al diseño de tablas](https://docs.aws.amazon.com/redshift/latest/dg/t_Creating_tables.html)
- [Sintaxis para crear tabla](https://docs.aws.amazon.com/redshift/latest/dg/r_CREATE_TABLE_NEW.html)
- [Tipo de datos](https://docs.aws.amazon.com/redshift/latest/dg/c_Supported_data_types.html)
- [Diseño avanzado de tablas](https://aws.amazon.com/blogs/big-data/amazon-redshift-engineerings-advanced-table-design-playbook-preamble-prerequisites-and-prioritization/)
- [Pasos para autorizar acceso cluster a otros servicios AWS](https://docs.aws.amazon.com/redshift/latest/mgmt/authorizing-redshift-service.html)
- [Herramientas recomendadas para conectarte a Redshift](https://www.blendo.co/amazon-redshift-guide-data-analyst/import-and-export-data/tools-to-connect-to-amazon-redshift-cluster/)
    - [Documentacion de AWS acerda de herramientas](https://docs.aws.amazon.com/redshift/latest/mgmt/connecting-via-client-tools.html)
